package com.slavik.activitywithfeedback;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button bt_choose ;
    TextView tv_side;

    private static final int FEEDBACK_FROM_CHOOSE_ACTIVITY = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv_side = findViewById(R.id.tv_side);

        bt_choose = findViewById(R.id.bt_choose);
        bt_choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ChooseActivity.class);
                startActivityForResult(intent,FEEDBACK_FROM_CHOOSE_ACTIVITY);
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if( requestCode == FEEDBACK_FROM_CHOOSE_ACTIVITY) {
            if (data != null) {
                tv_side.setText(data.getStringExtra(ChooseActivity.YOUR_SIDE));
            }
        }
    }


}
