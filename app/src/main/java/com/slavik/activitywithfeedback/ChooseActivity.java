package com.slavik.activitywithfeedback;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ChooseActivity extends AppCompatActivity {

    public static final String YOUR_SIDE = "com.slavik.activitywithfeedback.your_side";

    private final String DARK_SIDE = "Dark side";
    private final String WHITE_SIDE = "White side";

    Button bt_white;
    Button bt_dark;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose);

        bt_white = findViewById(R.id.bt_white_side);
        bt_dark = findViewById(R.id.bt_dark_side);

        bt_white.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent data = createIntent(WHITE_SIDE);
                setResult(RESULT_OK, data);
            }
        });

        bt_dark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent data = createIntent(DARK_SIDE);
                setResult(RESULT_OK, data);
            }
        });

    }

    private Intent createIntent(String message){
        Intent intent = new Intent(ChooseActivity.this, MainActivity.class);
        intent.putExtra(YOUR_SIDE,message);

        return intent;

    }

}
